# check_monit-tpt
A plugin to get xml from monit and parse it for CPU and memory stats

## Usage
<pre>
check_monit_tpt.py -H &lt;host&gt; -u &lt;user&gt; -P &lt;password&gt; -S &lt;service&gt; &lt;-C or -M or -S&gt; -w &lt;warning&gt; -c &lt;critical&gt;
</pre>

## Examples

* List all services being watched by Monit:
<pre>
./check_monit_tpt.py -H &lt;host&gt; -u &lt;username&gt; -P &lt;password&gt;
</pre>


* Apache CPU Usage:
<pre>
./check_monit_tpt.py -H &lt;host&gt; -u &lt;username&gt; -P &lt;password&gt; -s httpd -C -w 40 -c 50
</pre>


* MySQL Memory Usage:
<pre>
./check_monit_tpt.py -H &lt;host&gt; -u &lt;username&gt; -P &lt;password&gt; -s httpd -C -w 40 -c 50
</pre>



